### Hi there 👋

#### I'm a DevSecOps Engineer working in Toronto, Canada.

##### What I'm looking at:
- Exploring Toronto :) 

##### What I was looking at:
- ~~[Gitpod](https://www.gitpod.io/)~~
- ~~[vCluster](https://www.vcluster.com/)~~
- ~~[Python on Udemy](https://www.udemy.com/share/103IHM3@D8FLhsF45KG4Gr3MwrYrcG989U7rwSliBfM_7sYhiI0584L3bI8bKtUSMJ7H0RHdFg==/)~~
- ~~✨ Serverless [Vault](https://www.hashicorp.com/products/vault) - PAYG Vault~~
- ~~🚧 Remote x86 image builder~~
- ~~🧑‍💻 Weekly bouts of [Leetcode](https://gitlab.com/emctl/leetcode)~~
- ~~🕸 Updating my [website](https://emctl.gitlab.io) where I hope to post all my projects~~
- ~~Building out a website using Astro~~
- ~~Testing out [Pulumi](https://www.pulumi.com/)~~
- ~~Researching Clean Architecture and Domain Driven Design~~




##### Bio:

- Love all things Cloud / Kubernetes
- CKA / CKAD / AWS DOP / AWS SAP certified 
- Please checkout my [LinkedIn](https://www.linkedin.com/in/eamonn-mccudden-45b5425b/) if you want some background and feel free to reach out!
